import axios from 'axios';

export function createUser(user) {
    return {
        type: "CREATE_USER",
        payload: axios.post(`${process.env.REACT_APP_HOST}/user/add/`, user)
    }
}
export function fetchUser() {
    return {
        type: "FETCH_USERS",
        payload: axios.get(`${process.env.REACT_APP_HOST}/user/all`)
    }
}
export function fetchUserById(id) {
    return {
        type: "FETCH_USER_BY_ID",
        payload: axios.get(`${process.env.REACT_APP_HOST}/user/get/${id}`)
    }
}
export function updateUser(id, user) {
    return {
        type: "UPDATE_USER_BY_ID",
        payload: axios.put(`${process.env.REACT_APP_HOST}/user/update/${id}`, user)
    }
}
export function deleteUser(id) {
    return {
        type: "DELETE_USER",
        payload: axios.delete(`${process.env.REACT_APP_HOST}/user/delete/${id}`)
    }
}
export function addToTeam(id, users) {
        return {
            type: "ADD_TO_TEAM",
            payload: axios.post(`${process.env.REACT_APP_HOST}/user/add/users/team/${id}`, users)
        }
}
export function getUserTeams(id) {
    return {
        type: "GET_USER_TEAMS",
        payload: axios.get(`${process.env.REACT_APP_HOST}/${id}/teams`)
    }
}
export function getUsersNotInTeam(id) {
    return {
        type: "GET_USERS_NOT_IN_TEAM",
        payload: axios.get(`${process.env.REACT_APP_HOST}/user/users/outside/team/${id}`)
    }
}
export function getUserTournaments(id) {
    return {
        type: "GET_USER_TOURNAMENTS",
        payload: axios.get(`${process.env.REACT_APP_HOST}/game/get/with-user/${id}`)
    }
}
