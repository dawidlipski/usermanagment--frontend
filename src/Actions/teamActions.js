import axios from 'axios';

export function createTeam(team) {
    return {
        type: "CREATE_TEAM",
        payload: axios.post(`${process.env.REACT_APP_HOST}/team/add/with-users`, team)
    }
}
export function fetchTeams() {
    return {
        type: "FETCH_TEAMS",
        payload: axios.get(`${process.env.REACT_APP_HOST}/team/all`)
    }
}
export function fetchTeamById(id) {
    return {
        type: "FETCH_TEAM_BY_ID",
        payload: axios.get(`${process.env.REACT_APP_HOST}/team/get/${id}`)
    }
}
export function updateTeam(id, team) {
    return {
        type: "UPDATE_TEAM_BY_ID",
        payload: axios.put(`${process.env.REACT_APP_HOST}/team/update/${id}`, team)
    }
}
export function deleteTeam(id) {
    return {
        type: "DELETE_TEAM",
        payload: axios.delete(`${process.env.REACT_APP_HOST}/team/delete/${id}`)
    }
}
export function getUsersFromTeam(id) {
    return {
        type: "GET_USERS_FROM_TEAM",
        payload: axios.get(`${process.env.REACT_APP_HOST}/team/${id}/users`)
    }
}
