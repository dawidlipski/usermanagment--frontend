import React, { Component } from 'react';
import './App.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './constLayout/menu.css'
import './constLayout/scrollbar.css';
import './constLayout/layout.css';
import Menu from './constLayout/menu.js';

class App extends Component {

  constructor(props) {
      super(props);
    }

  render() {
    return (
      <div className="p-g p-g-nopad">
        <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
          <Menu />
        </div>
        <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
          <div className="main-label"><b>Simplify Tournament Managment</b></div>
          <div className="main-button" onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create`}><span>Create Tournament</span></div>
          <div className="bracket"></div>
        </div>
        <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{height: "70px"}}></div>
      </div>
    );
  }
}

export default App;
