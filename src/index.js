import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';
import {createStore, applyMiddleware} from 'redux';
import reducers from './Reducer';
import {Provider} from 'react-redux';
import {logger} from 'redux-logger';
import promiseMiddleware from 'redux-promise-middleware';
import ReduxThunk from 'redux-thunk';
import App from './App';
import ShowUsers from './Layout/showUsers';
import CreateUser from './Layout/createUser';
import EditUser from './Layout/editUser';
import CreateTeam from './Layout/createTeam';
import ShowTeams from './Layout/showTeams';
import EditTeam from './Layout/editTeam';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

const store = createStore(
  reducers,
  applyMiddleware(logger, ReduxThunk, promiseMiddleware())
)

ReactDOM.render(
    <Provider store={store}>
      <HashRouter basename="/usrmgmt">
      <Switch>
        <Route path="/show" component={ShowUsers}/>
        <Route path="/create" component={CreateUser}/>
        <Route path="/createteam" component={CreateTeam}/>
        <Route path="/showteams" component={ShowTeams}/>
        <Route path="/edit/:id" component={EditUser}/>
        <Route path="/editteam/:id" component={EditTeam}/>
        <Route path="/" component={App}/>
      </Switch>
      </HashRouter>
    </Provider>
  , document.getElementById('root'));
