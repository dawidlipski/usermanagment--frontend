import React, { Component } from 'react';
import './CSS/show.css';
import { bindActionCreators } from 'redux';
import * as userActions from '../Actions/userActions';
import { connect } from 'react-redux';
import RenderMenu from '../constLayout/menu.js';
import { Card } from 'primereact/card';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { Growl } from 'primereact/growl';
import { Button } from 'primereact/button';
import WOW from 'wowjs';
import ProgressBar from './progressBar';

class ShowUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      search: ""
    }
  }

  componentDidMount() {
    this.props.actions.fetchUser();
  }

  showDeleteInfo() {
    this.growl.show({ severity: 'success', summary: 'Success!', detail: 'User was deleted' });
  }

  deleteUser(id) {
    this.props.actions.deleteUser(id)
      .then(() => {
        this.props.actions.fetchUser()
          .then(() => this.showDeleteInfo())
          .catch(() => this.growl.show({
            severity: 'error', summary: 'Cannot delete user',
            detail: 'Check your connection with server'
          }));
      })
  }

  getUserTournaments(id) {
    this.props.actions.getUserTournaments(id);
  }

  hasBot(bot) {
    if (bot == '') {
      return 'No';
    } else {
      return 'Yes';
    }
  }

  componentDidUpdate() {
    if (!this.state.init) {
      new WOW.WOW().init();
      this.setState({ init: true })
    }
  }

  renderUsers() {

    let filteredUsers = this.props.user.userList;
    filteredUsers = filteredUsers.filter(
      (user) => {
        return user.nick.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      }
    )
    return filteredUsers.map((user) => {
      return (
        <section class="wow zoomIn" data-wow-iteration="1" data-wow-offset="40" data-wow-delay=".2s">
          <div class="userCard ">
            <div class="post-module hover" style={{ height: '100%' }}>
              <div class="post-content">
                <div class="category"><i>{user.name} {user.surname}</i>  </div>
                <h1 class="title">{user.nick}</h1>
                <h2 class="sub_title">Email: {user.email}</h2>
                <p class="description">
                  <table style={{ "margin": "0 auto", "width": "100%", "text-align": "middle" }}>
                    <tr><td><p>User Id:</p></td> <td><p>{user.id}</p></td></tr>
                    <tr><td><p>Has bot:</p></td> <td><p>{this.hasBot(user.bot)}</p></td></tr>
                  </table>
                </p>
                <div class="post-meta">
                  <Button label="Edit" className="myBbutton button1" onClick={() => this.props.history.replace(`/edit/${user.id}`)} />
                  <Button onClick={() => { this.deleteUser(user.id) }} label="Delete" className="myBbutton button3" />
                </div>
              </div>
            </div>
          </div>
        </section>
      );
    })
  }


  render() {
    if (!this.props.user.userList) {
      return (
        <ProgressBar />
      )
    } else {
      return (
        <div className="div-app div-app-show">
          <Growl ref={(el) => this.growl = el} />
          <div className="p-g p-g-nopad">
            <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
              <RenderMenu />
            </div>
            <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
              <input type="text" placeholder="Search by nickname..." className="searchBar"
                value={this.state.search} onChange={(event) => this.setState({ search: event.target.value })} style={{ "width": "95%" }}></input>
              {this.renderUsers()}
            </div>
            <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{ height: "70px" }}></div>
          </div>

        </div>
      );
  }
}
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(userActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowUsers);
