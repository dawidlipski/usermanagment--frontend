import React, { Component } from 'react';
import './CSS/create-user.css';
import { bindActionCreators } from 'redux';
import * as userActions from '../Actions/userActions';
import { connect } from 'react-redux';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import { InputText } from 'primereact/inputtext';
import '../constLayout/scrollbar.css';
import { ToggleButton } from 'primereact/togglebutton';
import { Growl } from 'primereact/growl';
import RenderMenu from '../constLayout/menu';
import { Button } from 'primereact/button';

class CreateUser extends Component {

  constructor(props) {
    super(props);
    this.state = {
      "name": '',
      "surname": '',
      "email": '',
      "nick": '',
      "bot": '',
      "cardId": '',
      "password": '',
      addBot: false,
      isButtonDisabled: false
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({
      addBot: !this.state.addBot
    })
  }

  userCreated() {
    this.growl.show({ severity: 'success', summary: 'User created', detail: '' });
    setTimeout(() => this.props.history.replace("/show"), 500);
  }

  createUser() {

    this.setState({nick: this.state.nick.replace(/\s/g, '')});
    this.setState({cardId: this.state.cardId.replace(/\s/g, '')});
    
    if (this.state.nick === '' || this.state.cardId === '') {
      this.growl.show({ severity: 'error', summary: 'Fulfill form', detail: 'Fields nick and cardID are required!' });
    } else if (this.state.cardId.length < 10) {
      this.growl.show({ severity: 'error', summary: 'Card ID incorrect', detail: 'Card ID must have more than 10 numbers!' });
    } else if (this.state.nick.length > 25) {
      this.growl.show({ severity: 'error', summary: 'Too long nick', detail: 'Nick can only have 25 characters!' });
    } else if (this.state.name.length > 25) {
      this.growl.show({ severity: 'error', summary: 'Too long name', detail: 'Name can only have 25 characters!' });
    } else if (this.state.surname.length > 25) {
      this.growl.show({ severity: 'error', summary: 'Too long last name', detail: 'Last name can only have 25 characters!' });
    } else {

      this.setState({
        isButtonDisabled: true
      });

      let user = {
        "name": this.state.name,
        "surname": this.state.surname,
        "email": this.state.email,
        "nick": this.state.nick,
        "bot": this.state.bot,
        "cardId": this.state.cardId
      }

      this.props.actions.createUser(user)
        .then(() => this.userCreated())
        .catch(() => {
          this.growl.show({ life: '6000', severity: 'error', summary: 'Something went wrong', detail: 'Couldnt create new user, try again with different nick or card ID and check your connection with server' });
          this.setState({isButtonDisabled: false});
        }
      );
    }
  }

  render() {

    return (
      <div className="p-g p-g-nopad">

        <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad render-menu height1">
          <RenderMenu />
        </div>

        <div className="p-lg-12 p-md-12 p-sm-12 p-g-nopad content height2">

          <Growl ref={(el) => this.growl = el}></Growl>

          <div className="my-form">

            <h1 className="welcome-text">Create New User</h1>

            <div className="form-row">
              <p className="form-text">User Name</p>

              <div className="p-g p-fluid">
                <div className="p-inputgroup div-input-double">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-user"></i>
                  </span>
                  <InputText
                    keyfilter="alpha" placeholder="Enter first name" className="input-text"
                    onChange={(event) => this.setState({ name: event.target.value })}
                    value={this.state.name} />
                </div>

                <div className="p-inputgroup div-input-double div-input-second">
                  <span className="p-inputgroup-addon">
                    <i className="pi pi-user"></i>
                  </span>
                  <InputText keyfilter="alpha" placeholder="Enter last name" className="input-text"
                    onChange={(event) => this.setState({ surname: event.target.value })}
                    value={this.state.surname} />
                </div>
              </div>
            </div>

            <div className="form-row">
              <p className="form-text">Nick</p>

              <div className="p-g p-fluid">
                <div className="p-inputgroup div-input">
                  <span className="p-inputgroup-addon">
                    <i className="pi">N</i>
                  </span>
                  <InputText keyfilter="alphanum" placeholder="Enter nickname" className="input-text"
                    onChange={(event) => this.setState({ nick: event.target.value })}
                    value={this.state.nick} />
                </div>
              </div>
            </div>

            <div className="form-row">
              <p className="form-text">Card ID</p>

              <div className="p-g p-fluid">
                <div className="p-inputgroup div-input">
                  <span className="p-inputgroup-addon">
                    <i className="pi">C</i>
                  </span>
                  <InputText keyfilter="pnum" placeholder="Enter card ID" className="input-text"
                    onChange={(event) => this.setState({ cardId: event.target.value })}
                    value={this.state.cardId} tooltip="In order to enter card ID, use NFC reader"
                    tooltipOptions={{position: 'top', event: 'focus'}} />
                </div>
              </div>
            </div>

            <div className="form-row">
              <p className="form-text">Email</p>

              <div className="p-g p-fluid">
                <div className="p-inputgroup div-input">
                  <span className="p-inputgroup-addon">
                    <i className="pi">E</i>
                  </span>
                  <InputText keyfilter="email" placeholder="Enter email" className="input-text"
                    onChange={(event) => this.setState({ email: event.target.value })}
                    value={this.state.email} />
                </div>
              </div>
            </div>

            <div className="p-g p-fluid div-bot">
              <div className="div-button-bot">
                <ToggleButton onLabel="Don't create bot" offLabel="Create bot" onIcon="pi pi-times" offIcon="pi pi-check"
                  checked={this.state.addBot} onChange={this.handleChange} style={{backgroundColor: '#FF7F28', border: 'none', color: 'white'}} />
              </div>

              {
                this.state.addBot
                  ?
                  <div className="div-input-bot div-input-double">
                    <InputText keyfilter={/[^\s]/} placeholder="Enter bot's URL" className="input-text"
                      onChange={(event) => this.setState({ bot: event.target.value })}
                      value={this.state.bot} />
                  </div>
                  : null
              }
            </div>
            <div className="button-create">
              <Button label="Create user" onClick={() => this.createUser()} disabled={this.state.isButtonDisabled}
              style={{height: '4em', width: '15em', backgroundColor: '#FF7F28', border: 'none'}} />
            </div>
          </div>
        </div>

        <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer"></div>
      </div>

    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(userActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);
