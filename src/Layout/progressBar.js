import React, { Component } from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';
import RenderMenu from '../constLayout/menu.js';
import './CSS/show.css';
import 'primereact/resources/primereact.min.css';

class ProgressBar extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="p-g p-g-nopad">
                <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
                    <RenderMenu />
                </div>
                <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
                    <div className="main-label">
                        <ProgressSpinner strokeWidth="5" /> <br />
                        Loading data...
                    </div>
                </div>
                <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{ height: "70px" }}></div>
            </div>
        );
    }
}
export default ProgressBar
