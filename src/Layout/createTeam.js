import React, { Component } from 'react';
import './CSS/create-team.css';
import { bindActionCreators } from 'redux';
import * as teamActions from '../Actions/teamActions';
import * as userActions from '../Actions/userActions';
import { connect } from 'react-redux';
import RenderMenu from '../constLayout/menu';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { PickList } from 'primereact/picklist';
import { Growl } from 'primereact/growl';
import ProgressBar from './progressBar';


class CreateTeam extends Component {

  constructor(props) {
    super(props);
    this.state = {
      group_name: '',
      source: [],
      target: [],
      showSourceControls: false,
      showTargetControls: false,
      isButtonDisabled: false
    }

    this.userTemplate = this.userTemplate.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.userActions.fetchUser()
      .then((res) => this.setState({ ...this.state, source: this.props.user.userList }));
  }

  onChange(event) {
    this.setState({
      source: event.source,
      target: event.target
    });
  }

  teamCreated() {
    this.growl.show({ severity: 'success', summary: 'Team created', detail: '' });
    setTimeout(() => this.props.history.replace("/showteams"), 1000);
  }

  createTeam() {

    this.setState({group_name: this.state.group_name.replace(/\s/g, '')});

    if (this.state.group_name === '') {
      this.growl.show({ severity: 'error', summary: 'Enter team name', detail: '' });
    } else if (this.state.group_name.length > 25) {
      this.growl.show({ severity: 'error', summary: 'Too long team name', detail: 'Team name can only have 25 characters!' });
    } else {

      this.setState({
        isButtonDisabled: true
      });

      let newTeam = {
        group_name: this.state.group_name,
        users: this.state.target.slice()
      }

      this.props.teamActions.createTeam(newTeam)
        .then(() => this.teamCreated())
        .catch(() => {
          this.growl.show({ severity: 'error', summary: 'Error', detail: 'Couldnt create new team' });
          this.setState({isButtonDisabled: false});
          }
        )
    }
  }

  userTemplate(user) {
    return (
      <div className="ui-helper-clearfix">
        <div className="div-user-list"> {user.nick} </div>
      </div>
    );
  }

  render() {
    if (!this.props.user.userList) {
      return (
        <ProgressBar />
      )
    } else {

      return (
        <div className="p-g p-g-nopad">

          <Growl ref={(el) => this.growl = el}></Growl>

          <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad render-menu height1">
            <RenderMenu />
          </div>

          <div className="p-lg-12 p-md-12 p-sm-12 p-g-nopad content height2">
            <div className="my-form">

              <h1 className="welcome-text">Create New Team</h1>

              <div className="form-row">
                <p className="form-text">Team Name</p>

                <div className="p-g p-fluid">
                  <div className="p-inputgroup div-input">
                    <InputText keyfilter="alpha" placeholder="Enter name of a team"
                      className="input-text"
                      onChange={(event) => this.setState({ group_name: event.target.value })}
                      value={this.state.group_name} />
                  </div>
                </div>
              </div>

              <div className="picklist-users">
                <PickList source={this.state.source} target={this.state.target} itemTemplate={this.userTemplate}
                  sourceHeader="Users available"
                  targetHeader="Users in team"
                  showSourceControls={this.state.showSourceControls}
                  showTargetControls={this.state.showTargetControls}
                  sourceStyle={{ height: '22em' }}
                  targetStyle={{ height: '22em' }}
                  onChange={this.onChange}
                  responsive={true} >
                </PickList>
              </div>

              <div className="button-create">
                <Button label="Create team" onClick={() => this.createTeam()} disabled={this.state.isButtonDisabled}
                  style={{ height: '4em', width: '15em', backgroundColor: '#FF7F28', border: 'none' }} />
              </div>
            </div>

          </div>

          <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer"></div>

        </div>

      )

    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    teamActions: bindActionCreators(teamActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    team: state.team
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTeam);