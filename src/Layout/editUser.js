import React, { Component } from 'react';
import './CSS/create-user.css';
import { bindActionCreators } from 'redux';
import * as userActions from '../Actions/userActions';
import { connect } from 'react-redux';
import { InputText } from 'primereact/inputtext';
import '../constLayout/scrollbar.css';
import { Growl } from 'primereact/growl';
import RenderMenu from '../constLayout/menu';
import { Button } from 'primereact/button';
import ProgressBar from './progressBar';

class EditUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            "name": '',
            "surname": '',
            "email": '',
            "nick": '',
            "bot": '',
            "cardId": '',
            addBot: false,
            isButtonDisabled: false
        }
        this.deleteBot = this.deleteBot.bind(this);
    }

    componentDidMount() {
        this.props.actions.fetchUserById(this.props.match.params.id);
    }

    // updateMyUser() {
    //     this.createUser();
    //     this.props.actions.fetchUser();
    // }

    deleteBot() {
        let name = (this.state.name === '') ? this.props.user.user.name : this.state.name;
        let surname = (this.state.surname === '') ? this.props.user.user.surname : this.state.surname;
        let email = (this.state.email === '') ? this.props.user.user.email : this.state.email;
        let nick = (this.state.nick === '') ? this.props.user.user.nick : this.state.nick;
        let cardId = (this.state.cardId === '') ? this.props.user.user.cardId : this.state.cardId;
        let bot = '';

        let user = {
            "name": name,
            "surname": surname,
            "email": email,
            "nick": nick,
            "bot": bot,
            "cardId": cardId
        }
        this.props.actions.updateUser(this.props.match.params.id, user)
            .then(() => this.props.actions.fetchUserById(this.props.match.params.id))
            .catch(() => this.growl.show({ severity: 'error', summary: 'Error', detail: 'Cannot delete bot' }));
    }

    userUpdated() {
        this.growl.show({ severity: 'success', summary: 'User updated', detail: '' });
        setTimeout(() => this.props.history.replace("/show"), 500);
    }

    createUser() {

        this.setState({ nick: this.state.nick.replace(/\s/g, '') });
        this.setState({ cardId: this.state.cardId.replace(/\s/g, '') });

        let name = (this.state.name === '') ? this.props.user.user.name : this.state.name;
        let surname = (this.state.surname === '') ? this.props.user.user.surname : this.state.surname;
        let email = (this.state.email === '') ? this.props.user.user.email : this.state.email;
        let nick = (this.state.nick === '') ? this.props.user.user.nick : this.state.nick;
        let bot = (this.state.bot === '') ? this.props.user.user.bot : this.state.bot;
        let cardId = (this.state.cardId === '') ? this.props.user.user.cardId : this.state.cardId;

        let user = {
            "name": name,
            "surname": surname,
            "email": email,
            "nick": nick,
            "bot": bot,
            "cardId": cardId
        }

        if (this.state.cardId.length > 15) {
            this.growl.show({ severity: 'error', summary: 'Card ID incorrect', detail: 'Card ID can have max 15 characters!' });
        } else if (this.state.nick.length > 25) {
            this.growl.show({ severity: 'error', summary: 'Too long nick', detail: 'Nick can only have 25 characters!' });
        } else if (this.state.name.length > 25) {
            this.growl.show({ severity: 'error', summary: 'Too long name', detail: 'Name can only have 25 characters!' });
        } else if (this.state.surname.length > 25) {
            this.growl.show({ severity: 'error', summary: 'Too long last name', detail: 'Last name can only have 25 characters!' });
        } else if (this.state.email.length > 25) {
            this.growl.show({ severity: 'error', summary: 'Too long email', detail: 'Email can only have 25 characters!' });
        } else {

            this.setState({
                isButtonDisabled: true
            });

            return (
                this.props.actions.updateUser(this.props.match.params.id, user)
                    .then(() => {
                        this.userUpdated();
                    })
                    .catch(() => {
                        <Growl ref={(el) => this.growl = el}></Growl>
                        this.growl.show({ severity: 'error', summary: 'Cannot update user', detail: 'Check your connection with server' })
                        this.setState({ isButtonDisabled: false });
                    })
            );
        }
    }

    renderUser() {
        if (!this.props.user.user) {
            return (
                <ProgressBar />
            )
        } else {

            return (
                <div className="p-g p-g-nopad">

                    <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad render-menu height1">
                        <RenderMenu />
                    </div>

                    <div className="p-lg-12 p-md-12 p-sm-12 p-g-nopad content height2">

                        <Growl ref={(el) => this.growl = el}></Growl>

                        <div className="my-form">

                            <h1 className="welcome-text">Edit User</h1>

                            <div className="form-row">
                                <p className="form-text">User Name</p>

                                <div className="p-g p-fluid">
                                    <div className="p-inputgroup div-input-double">
                                        <span className="p-inputgroup-addon">
                                            <i className="pi pi-user"></i>
                                        </span>
                                        <InputText
                                            keyfilter="alpha" placeholder="Enter first name" className="input-text"
                                            onChange={(event) => this.setState({ name: event.target.value })}
                                            value={this.state.name} placeholder={this.props.user.user.name} />
                                    </div>

                                    <div className="p-inputgroup div-input-double div-input-second">
                                        <span className="p-inputgroup-addon">
                                            <i className="pi pi-user"></i>
                                        </span>
                                        <InputText keyfilter="alpha" placeholder="Enter last name" className="input-text"
                                            onChange={(event) => this.setState({ surname: event.target.value })}
                                            value={this.state.surname} placeholder={this.props.user.user.surname} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <p className="form-text">Nick</p>

                                <div className="p-g p-fluid">
                                    <div className="p-inputgroup div-input">
                                        <span className="p-inputgroup-addon">
                                            <i className="pi">N</i>
                                        </span>
                                        <InputText keyfilter="alphanum" placeholder="Enter nickname" className="input-text"
                                            onChange={(event) => this.setState({ nick: event.target.value })}
                                            value={this.state.nick} placeholder={this.props.user.user.nick} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <p className="form-text">Card ID</p>

                                <div className="p-g p-fluid">
                                    <div className="p-inputgroup div-input">
                                        <span className="p-inputgroup-addon">
                                            <i className="pi">C</i>
                                        </span>
                                        <InputText keyfilter="pnum" placeholder="Enter card ID" className="input-text"
                                            onChange={(event) => this.setState({ cardId: event.target.value })}
                                            value={this.state.cardId} placeholder={this.props.user.user.cardId} />
                                    </div>
                                </div>
                            </div>

                            <div className="form-row">
                                <p className="form-text">Email</p>

                                <div className="p-g p-fluid">
                                    <div className="p-inputgroup div-input">
                                        <span className="p-inputgroup-addon">
                                            <i className="pi">E</i>
                                        </span>
                                        <InputText keyfilter="email" placeholder="Enter email" className="input-text"
                                            onChange={(event) => this.setState({ email: event.target.value })}
                                            value={this.state.email} placeholder={this.props.user.user.email} />
                                    </div>
                                </div>
                            </div>

                            <div className="p-g p-fluid div-bot">
                                {
                                    this.props.user.user.bot === ''
                                        ?
                                        <div className="form-row">
                                            <h3 className="form-text" style={{ height: '30px' }}>User doesn't have bot</h3>
                                            <h3 className="form-text">Enter bot's URL in order to assign bot</h3>
                                            <InputText keyfilter={/[^\s]/} className="input-text"
                                                onChange={(event) => this.setState({ bot: event.target.value })}
                                                value={this.state.bot} />
                                        </div>
                                        :
                                        <div className="form-row">
                                            <h3 className="form-text" style={{ height: '30px' }}>User has bot</h3>

                                            <InputText keyfilter={/[^\s]/} placeholder="Enter bot's URL to update" className="input-text"
                                                onChange={(event) => this.setState({ bot: event.target.value })}
                                                value={this.state.bot} placeholder={this.props.user.user.bot} />
                                        </div>
                                }
                            </div>
                            <div className="button-create">
                                <Button label="Update user" onClick={() => this.createUser()} disabled={this.state.isButtonDisabled}
                                    style={{ height: '4em', width: '15em', backgroundColor: '#FF7F28', border: 'none' }} />
                                {
                                    this.props.user.user.bot === ''
                                        ?
                                        null
                                        :
                                        <Button label="Delete bot" onClick={() => this.deleteBot()}
                                            style={{ height: '4em', width: '15em', backgroundColor: '#FF7F28', border: 'none', marginLeft: '2em' }} />

                                }
                                <Button label="Go to users" onClick={() => this.props.history.replace(`/show`)}
                                    style={{ height: '4em', width: '15em', backgroundColor: '#FF7F28', border: 'none', marginLeft: '2em' }} />

                            </div>
                        </div>
                    </div>

                    <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer"></div>
                </div>

            )
        }

    }

    render() {
        return (
            <div>
                {this.renderUser()}
            </div>
        );
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);
