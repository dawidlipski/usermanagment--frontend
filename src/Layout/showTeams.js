import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import * as teamActions from '../Actions/teamActions';
import './CSS/show.css';
import { connect } from 'react-redux';
import Menu from '../constLayout/menu.js';
import { Card } from 'primereact/card';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { Growl } from 'primereact/growl';
import { Button } from 'primereact/button';
import WOW from 'wowjs';
import ProgressBar from './progressBar';

class ShowTeams extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      search: "",
      init: false
    }
  }

  componentDidMount() {
    this.props.actions.fetchTeams();
  }

  showDeleteInfo() {
    this.growl.show({ severity: 'success', summary: 'Success!', detail: 'Team was deleted' });
  }

  deleteTeam(id) {
    this.props.actions.deleteTeam(id)
      .then(() => {
        this.props.actions.fetchTeams();
        this.showDeleteInfo();
      })
  }

  renderTeams() {

    let filteredTeams = this.props.team.teamList;
    filteredTeams = filteredTeams.filter(
      (team) => {
        if (team.group_name != null || this.state.search != null)
          return team.group_name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      }
    )
    return filteredTeams.map((team) => {
      return (
        <section class="wow zoomIn" data-wow-iteration="1" data-wow-offset="40" data-wow-delay=".2s">
          <div class="userCard">
            <div class="post-module hover" style={{ height: '100%' }}>
              <div class="post-content">
                <div class="category">Team ID: <i>{team.id}</i> </div>
                <h1 class="title teamName">{team.group_name}</h1>
                <div class="post-meta" style={{ "padding-top": "120px!important" }}>
                  <Button label="Edit" className="myBbuttonTeam button1Team" onClick={() => this.props.history.replace(`/editteam/${team.id}`)} />
                  <Button label="Delete" className="myBbuttonTeam button1Team" onClick={() => { this.deleteTeam(team.id) }} />
                </div>
              </div>
            </div>
          </div>
        </section>
      );
    })

  }

  componentDidUpdate() {
    if (!this.state.init) {
      new WOW.WOW().init();
      this.setState({ init: true })
    }
  }

  render() {
    if (!this.props.team.teamList) {
      return (
        <ProgressBar />
      )
    } else {
      return (
        <div className="div-app div-app-show">
          <Growl ref={(el) => this.growl = el} />
          <div className="p-g p-g-nopad">
            <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
              <Menu />
            </div>
            <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
              <input type="text" placeholder="Search by team name..." className="search-bar"
                value={this.state.search} onChange={(event) => this.setState({ search: event.target.value })} style={{ "width": "95%" }}></input>
              {this.renderTeams()}
            </div>
            <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{ height: "70px" }}></div>
          </div>
        </div>
      );
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(teamActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    team: state.team
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowTeams);
