export default (state = {tournamentList: null}, action) => {
    switch(action.type) {
        case 'CREATE_USER_FULFILLED' : {
            return {
                state: action.payload.data
            }
        }
        case 'CREATE_USER_REJECTED' : {
            return {
                state: action.payload.data
            }
        }
        case 'FETCH_USERS_FULFILLED' : {
            return { 
            ...state,
            userList: action.payload.data
            }
        }
        case 'FETCH_USER_BY_ID_FULFILLED' : {
            return { 
            ...state,
            user: action.payload.data
            }
        }
        case 'UPDATE_USER_FULFILLED' : {
            return { 
            ...state,
            user: action.payload.data
            }
        }
        case 'DELETE_USER_FULFILLED' : {
            return action.payload.data
        }
        case 'ADD_TO_TEAM_FULFILLED' : {
            return action.payload.data
        }
        case 'GET_USER_TEAMS_FULFILLED' : {
            return action.payload.data
        }
        case 'GET_USERS_NOT_IN_TEAM_FULFILLED' : {
            return {
                userList: action.payload.data
            }
        }
        case 'GET_USER_TOURNAMENTS_FULFILLED' : {
            return {
                ...state,
                tournamentList: action.payload.data
            }
        }
        
        default : {
            return state;
        }
    }
}