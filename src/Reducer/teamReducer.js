export default (state = {}, action) => {
    switch(action.type) {
        case 'CREATE_TEAM_FULFILLED' : {
            return {
                state: action.payload.data
            }
        }
        case 'FETCH_TEAMS_FULFILLED' : {
            return { 
            ...state,
            teamList: action.payload.data
            }
        }
        case 'FETCH_TEAM_BY_ID_FULFILLED' : {
            return { 
            ...state,
            team: action.payload.data
            }
        }
        case 'UPDATE_TEAM_FULFILLED' : {
            return { 
            ...state,
            team: action.payload.data
            }
        }
        case 'DELETE_TEAM_FULFILLED' : {
            return action.payload.data
        }
        case 'GET_USERS_FROM_TEAM_FULFILLED' : {
            return {
            ...state,
            teamList: action.payload.data
            }
        }
        
        default : {
            return state;
        }
    }
}