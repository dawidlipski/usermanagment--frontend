import React, { Component } from 'react';
import SwipeableTemporaryDrawer from './SwipeableTemporaryDrawer.js';
import { Link } from 'react-router-dom';

class menu extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="burger">
          <SwipeableTemporaryDrawer style={{ fontFamily: 'Open Sans' }} />
        </div>

        <div className="menu">

          <ul>
            <li className="menu-item">
              <div className="menu-item-icon">
                <i className="pi pi-th-large" style={{ 'fontSize': '2em' }}></i>
              </div>
              <div className="menu-item-label">
                <span className="span-label">Tournaments</span>
                <ul>
                  <li onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create/user`}>Create User Tournament</li>
                  <li onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create/team`}>Create Team Tournament</li>
                  <li onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create/bot`}>Create Bot Tournament</li>
                </ul>
              </div>
              <hr />
            </li>
            <li className="menu-item">
              <div className="menu-item-icon">
                <i className="pi pi-users" style={{ 'fontSize': '2em' }}></i>
              </div>
              <div className="menu-item-label">
                <span className="span-label">Participants</span>
                <ul>
                  <Link to="/create"><li>Add User</li></Link>
                  <Link to="/show"><li>Manage Users</li></Link>
                  <Link to="/createteam"><li>Add team</li></Link>
                  <Link to="/showteams"><li>Manage teams</li></Link>
                </ul>
              </div>
              <hr />
            </li>
            <li className="menu-item">
              <div className="menu-item-icon">
                <i className="pi pi-globe" style={{ 'fontSize': '2em' }}></i>
              </div>
              <div className="menu-item-label" >
                <span onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/show`}>Games</span>
                </div>
              <hr />
            </li>
            <li className="menu-item">
              <div className="menu-item-icon">
                <i className="pi pi-info-circle" style={{ 'fontSize': '2em' }}></i>
              </div>
              <div className="menu-item-label">
              <span onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/statistics`}>Statitsics</span>
            </div>
              <hr />
            </li>
          </ul>

        </div>

      </div>
    )
  }
}


export default menu;
