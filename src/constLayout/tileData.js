// This file is shared across the demos.

import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Item = styled.div`
  color: #33ccff;
  font-size: 28px;
  margin-left: 10px;
  border-bottom: 1px solid white;
  margin-top: 20px;
  &:hover {
   background: rgba(100,100,100,0.6);
 }
`;

const SubItem = styled.div`
  color: white;
  font-size: 17px;
  margin-left: 40px;
  margin-bottom: 5px;
  &:hover {
   color: #33ccff;
 }
`;

class tileData extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>

        <Item>
          <i className="pi pi-th-large" style={{ 'fontSize': '28px' }}></i>Tournaments

        <SubItem onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create/user`}>Create User Tournament</SubItem>
          <SubItem onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create/team`}>Create Team Tournament</SubItem>
          <SubItem onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/create/bot`}>Create Bot Tournament</SubItem>

        </Item>

        <Item>
          <i className="pi pi-users" style={{ 'fontSize': '28px' }}></i>Participants

        <Link to="/create"><SubItem>Create User</SubItem></Link>
          <Link to="/show"><SubItem>Manage Users</SubItem></Link>
          <Link to="/createteam"><SubItem>Create Team</SubItem></Link>
          <Link to="/showteams"><SubItem>Manage Teams</SubItem></Link>
        </Item>

        <Item onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/show`}>
          <i className="pi pi-globe" style={{ 'fontSize': '28px' }}></i>Games
    </Item>
        <Item onClick={() => window.location = `${process.env.REACT_APP_TOURNAMENT_SYSTEM}/#/tournament/statistics`}>
          <i className="pi pi-info-circle" style={{ 'fontSize': '28px' }}></i>Statistics
    </Item>

      </div>
    )
  }
}

export default tileData;
